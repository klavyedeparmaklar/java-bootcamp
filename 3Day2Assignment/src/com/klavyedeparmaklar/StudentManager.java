package com.klavyedeparmaklar;

public class StudentManager {

    public void subscribeLesson(Student student){
        System.out.println(student.userName + " has successfully subscribe to the " + student.classRoom + " course!");
    }
}
