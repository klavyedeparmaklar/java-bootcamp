package com.klavyedeparmaklar;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Student sebahattin = new Student();
        sebahattin.userName = "sebahattin";

        Instructor engin = new Instructor();
        engin.userName = "engin";

        UserManager userManager = new UserManager();
        userManager.add(sebahattin);
        userManager.add(engin);

        System.out.println("**** All User ****");
        User[] users = {sebahattin, engin};
        userManager.addAllUsers(users);

        System.out.println("**** Subsbribe a Course ****");
        StudentManager studentManager = new StudentManager();
        sebahattin.classRoom = "Java";
        studentManager.subscribeLesson(sebahattin);

        System.out.println("**** Create a Course ****");
        InstructorManager instructorManager = new InstructorManager();
        engin.courseLanguage = "Java";
        instructorManager.createNewCourse(engin);
    }
}
