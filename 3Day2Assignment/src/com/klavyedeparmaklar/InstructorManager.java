package com.klavyedeparmaklar;

public class InstructorManager {

    public void createNewCourse(Instructor instructor){
        System.out.println(instructor.userName + " has created a new course, " + instructor.courseLanguage);
    }
}
