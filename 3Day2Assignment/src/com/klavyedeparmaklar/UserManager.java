package com.klavyedeparmaklar;

public class UserManager {

    public void add(User user){
        System.out.println("'" + user.userName + "`" + " has been added!");
    }

    public void addAllUsers(User[] users){
        for(User user : users){
            add(user);
        }
    }

}
