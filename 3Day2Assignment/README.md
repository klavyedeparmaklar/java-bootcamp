# Kodlama.io's 3rd lesson 2nd homework


Main class's code below!

```java

Student sebahattin = new Student();
sebahattin.userName = "sebahattin";

Instructor engin = new Instructor(); 
engin.userName = "engin";

UserManager userManager = new UserManager(); 
userManager.add(sebahattin); 
userManager.add(engin);

System.out.println("**** All User ****");
User[] users = {sebahattin, engin};
userManager.addAllUsers(users);

System.out.println("**** Subsbribe a Course ****");
StudentManager studentManager = new StudentManager();
sebahattin.classRoom = "Java";
studentManager.subscribeLesson(sebahattin);

System.out.println("**** Create a Course ****");
InstructorManager instructorManager = new InstructorManager();
engin.courseLanguage = "Java";
instructorManager.createNewCourse(engin);

```

Console result is

```

'sebahattin` has been added!
'engin` has been added!

**** All User ****
'sebahattin` has been added!
'engin` has been added!

**** Subsbribe a Course ****
sebahattin has successfully subscribe to the Java course!

**** Create a Course ****
engin has created a new course, Java

```

<details><summary>author's details...</summary>
<p>

```
     Sebahattin KALACH
        Software Artisan
```

</p>
</details>
